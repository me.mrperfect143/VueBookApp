from flask_restplus import Api,Resource
from flask import Flask,request
from application.books import bookBP, bookAPI
from application.users import userBP, userAPI
from application.users import users
from application.books import books
from flask_mongoengine import MongoEngine
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager,get_jwt,set_access_cookies
from flask_cors import CORS

from datetime import datetime
from datetime import timezone
from datetime import timedelta

jwt = JWTManager()
db = MongoEngine()

api = Api(
    version='1.0',
    title='Flask API for VueBooks API',
    contact="Shivakumar",
    contact_url='https://github.com/shivakumar-zugzwang',
    # doc="/doc",
    description='Contains Endpoints for Books and Users',
    default='API',
    default_label='',
)
def create_app():
    app = Flask(__name__)
    app.config["MONGODB_SETTINGS"] = {
        "host" : "mongodb+srv://Shivakumar:me.mrperfect143@cluster0.vmboy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    }
    db.init_app(app)
    app.config["JWT_COOKIE_SECURE"] = False
    app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)
    app.config["JWT_SECRET_KEY"] = "me.mrperfect143"

    jwt.init_app(app)
    api.init_app(app)
    app.register_blueprint(bookBP,url_prefix="/api/books")
    app.register_blueprint(userBP,url_prefix="/api/users")
    api.add_resource(books.Books,'/books/')
    api.add_resource(books.BookList,'/books/<BookID>')
    api.add_resource(users.Register,'/users/Register')
    api.add_resource(users.Login,'/users/Login')
    api.add_resource(users.Logout,'/users/Logout')
    api.add_namespace(bookAPI)
    api.add_namespace(userAPI)
    @app.after_request
    def refresh_expiring_jwts(response):
        try:
            exp_timestamp = get_jwt()["exp"]
            now = datetime.now(timezone.utc)
            target_timestamp = datetime.timestamp(now + timedelta(minutes=30))
            if target_timestamp > exp_timestamp:
                access_token = create_access_token(identity=get_jwt_identity())
                set_access_cookies(response, access_token)
            return response
        except (RuntimeError, KeyError):
            # Case where there is not a valid JWT. Just return the original respone
            return response

    CORS(app)
    return app