from flask import Blueprint
from flask_restplus import Api
userBP = Blueprint('users', __name__)

userAPI = Api(userBP, version='1.0', title='User API',doc="/doc", description="""sumary_line""",params={ 'id': 'Specify the Id associated with the person' },default="Users",default_label="Users API")
