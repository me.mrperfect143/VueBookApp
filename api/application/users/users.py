from flask_jwt_extended.utils import set_access_cookies
from ..models import User,PublicUsers
from flask import Blueprint, request, jsonify
from flask_restplus import Api, Resource, fields
import json
from application.users import userAPI
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity, get_jwt
from flask_jwt_extended import jwt_required, unset_jwt_cookies

registerData = userAPI.model(
    "registerData",
    {
        "first_name": fields.String(description="FirstName", required=True),
        "last_name": fields.String(description="LastName", required=True),
        "email": fields.String(description="Email", required=True),
        "password": fields.String(description="Password", required=True),
        "phone": fields.String(description="PhoneNumber", required=True),
        "address": fields.String(description="Address", required=True),
    },
)

loginData = userAPI.model(
    "loginData", {
        "email": fields.String(description="Email", required=True),
        "password": fields.String(description="Password", required=True),
    }
)



@userAPI.route('/Register')
class Register(Resource):
    @userAPI.expect(registerData)
    def post(self):
        exist = User.objects(email=request.json["email"]).first()
        if exist is not None:
            return {"message": "User already exists"}, 401
        public = PublicUsers()
        public.first_name = request.json['first_name']
        public.last_name = request.json['last_name']
        public.email = request.json['email']
        public.set_password(request.json['password'])
        public.phone = request.json['phone']
        public.address = request.json['address']
        public.save()
        return {'message': 'User created successfully'}, 201

@userAPI.route('/Login')
class Login(Resource):
    @userAPI.expect(loginData)
    def post(self):
        email = request.json['email']
        password = request.json['password']
        user = User.objects(email=email).first()
        if user and user.check_password(password):
            access_token = create_access_token(identity=user)
            response = jsonify({'message': 'User logged in successfully','access_token':access_token})
            set_access_cookies(response,access_token)
            return response
            # return 
        else:
            return {'message': 'Invalid username or password'}, 401

@userAPI.route('/Logout')
class Logout(Resource):
    def post(self):
        response = jsonify({"msg": "logout successful"})
        unset_jwt_cookies(response)
        return response
