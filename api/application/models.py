from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from mongoengine import *

class User(UserMixin,DynamicDocument):
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    email = StringField(max_length=50)
    password_hash = StringField(max_length=128)
    meta = {"allow_inheritance": True}
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

class PublicUsers(User):
    phone = StringField(max_length=255)
    address = StringField(max_length=255)


class Book(DynamicDocument):
    bookid = StringField(max_length=255)
    title = StringField(max_length=255)
    author = StringField(max_length=255)
    description = StringField(max_length=255)
    image = StringField(max_length=255)
    language = StringField(max_length=255)
    published = StringField(max_length=255)
    meta = {'collection': 'books'}