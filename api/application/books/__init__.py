from flask import Blueprint
from flask_restplus import Api
bookBP = Blueprint('book', __name__)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"
    }
}
bookAPI = Api(
    bookBP, 
    version='1.0', 
    authorizations=authorizations,
    title='Book API',
    doc="/doc", 
    default="Books Endpoints"
)
