from ..models import Book
from flask import Blueprint, request
from flask_restplus import Api, Resource, fields
import json
from application.books import bookAPI
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
bookData = bookAPI.model(
    "bookData",
    {
        "bookid": fields.String(description="ID for Books", required=True),
        "title": fields.String(description="Book Title", required=True),
        "author": fields.String(description="Book Author", required=True),
        "published": fields.String(description="Publisher Name", required=True),
        "description": fields.String(description="Book Description", required=True),
        "image": fields.String(description="ImageURL", required=True),
        "language": fields.String(description="Language of Book", required=True),
    },
)


@bookAPI.route('/getBooks')
class Books(Resource):
    @jwt_required()
    @bookAPI.doc(security='apikey')
    def get(self):
        books = Book.objects()
        return {'books': [json.loads(book.to_json()) for book in books]}
    
    @bookAPI.expect(bookData)
    @bookAPI.doc(security='apikey')
    @jwt_required()
    def post(self):
        book = Book()
        book.bookid = request.json['bookid']
        book.title = request.json['title']
        book.author = request.json['author']
        book.published = request.json['published']
        book.description = request.json['description']
        book.image = request.json['image']
        book.language = request.json['language']
        book.save()
        return {'message':"Books Stored Successfully"}

@bookAPI.route('/<BookID>')
class BookList(Resource):
    @jwt_required()
    @bookAPI.doc(security='apikey')
    def get(self,bookID):
        book = Book.objects(bookiid=bookID).first()
        if book:
            return {'book': book.to_json()}
        else:
            return {'message':"Books Not Found"}
            
    @bookAPI.expect(bookData)
    @jwt_required()
    @bookAPI.doc(security='apikey')
    def put(self,BookID):
        book = Book.objects(bookiid=BookID).first()
        if book:
            book.title = request.json['title']
            book.author = request.json['author']
            book.published = request.json['published']
            book.description = request.json['description']
            book.image = request.json['image']
            book.language = request.json['language']
            book.save()
            return {'message':"Books Updated Successfully"}
        else:
            return {'message':"Books Not Found"}

    @jwt_required()
    @bookAPI.doc(security='apikey')
    def delete(self,BookID):
        book = Book.objects(bookiid=BookID).first()
        if book:
            book.delete()
            return {'message':"Books Deleted Successfully"}
        else:
            return {'message':"Books Not Found"}

